# Building Properties

## Government Services

| Building Name   | X    | Y    | Price |
| :---            | ---: | ---: | ---:  |
| Town Hall       | 2    | 3    | 100   |
| City Hall       | 2    | 3    | 100   |
| City Storage    | 3    | 2    | 0     |
| Mayor's Mansion | 3    | 3    | 100   |

## Shop

| Building Name         | X    | Y    | Price | Requirement |
| :---                  | ---: | ---: | ---:  | ---:        |
| Building Supply Store | 2    | 2    | 100   |             |
| Hardware Store        | 2    | 2    | 2500  | Level 4     |
| Farmer's Market       | 2    | 2    | 5000  | Level 8     |
| Furniture Store       | 2    | 2    | 8000  | Level 10    |
| Gardening Supplies    | 2    | 2    | 13000 | Level 14    |
| Donut Shop            | 2    | 2    | 17000 | Level 18    |
| Fashion Store         | 2    | 2    | 22000 | Level 19    |
| Fast Food Restaurant  | 2    | 2    | 25000 | Level 25    |
| Home Appliances       | 2    | 2    | 30000 | Level 29    |

## Residential Building Properties

| Residential Type | X    | Y    | Populations | Requirement |
| :---             | ---: | ---: | ---:        | ---:        |
| Residential Zone | 2    | 2    | 1836        |             |
| Tokyo Town Zone  | 2    | 2    | 2204        | Airport     |
| London Town Zone | 2    | 2    | 2111        | Airport     |
| Parisian Zone    | 2    | 2    | 2019        | Airport     |

## Utility Services

### Power
| Building Name           | X    | Y    | PolutionX | PolutionY | Capacity | Price | Requirement       |
| :---                    | ---: | ---: | ---:      | ---:      | ---:     | ---:  | ---:              |
| Wind Power Plant        | 1    | 1    | 0         | 0         | 6        | 6000  |                   |
| Coal Power Plant        | 2    | 2    | 10        | 10        | 12       | 4500  |                   |
| Deluxe Wind Power Plant | 2    | 2    | 0         | 0         | 22       | 15000 | 2000 Populations  |
| Solar Power Plant       | 4    | 2    | 0         | 0         | 35       | 22000 | 23000 Populations |
| Oil Power Plant         | 4    | 2    | 8         | 8         | 40       | 15000 | 40000 Populations |
| Nuclear Power Plant     | 4    | 3    | 12        | 12        | 60       | 35000 | 70000 Populations |
| Fusion Power Plant      | 3    | 3    | 0         | 0         | 75       | 90000 | University        |

### Water
| Building Name         | X    | Y    | Capacity | Price | Requirement       |
| :---                  | ---: | ---: | ---:     | ---:  | ---:              |
| Basic Water Tower     | 1    | 1    | 9        | 6000  |                   |
| Water Pumping Station | 3    | 2    | 55       | 45000 | 15000 Populations |

### Sewage
| Building Name                 | X    | Y    | PolutionX | PolutionY | Capacity | Price | Requirement |
| :---                          | ---: | ---: | ---:      | ---:      | ---:     | ---:  | ---:        |
| Small Sewage Outflow Pipe     | 1    | 1    | 10        | 10        | 7        | 4000  | Level 8     |
| Basic Sewage Outflow Pipe     | 2    | 2    | 12        | 12        | 28       | 12000 | Level 8     |
| Deluxe Sewage Treatment Plant | 3    | 2    | 0         | 0         | 55       | 35000 | Level 8     |

### Waste Management
| Building Name       | X    | Y    | PolutionX | PolutionY | Capacity | Price | Requirement |
| :---                | ---: | ---: | ---:      | ---:      | ---:     | ---:  | ---:        |
| Small Garbage Dump  | 2    | 2    | 10        | 10        | 15       | 6000  | Level 14    |
| Garbage Dump        | 2    | 3    | 12        | 12        | 31       | 12000 | Level 14    |
| Garbage Incinerator | 2    | 2    | 16        | 16        | 40       | 12000 | Level 14    |
| Recycling Center    | 3    | 2    | 0         | 0         | 70       | 60000 | Level 14    |

## Coverage Services

### Fire
| Buidling Name       | X    | Y    | CoverX | CoverY | Price | Requirement |
| :---                | ---: | ---: | ---:   | ---:   | ---:  | :---        |
| Small Fire Station  | 1    | 1    | 6      | 8      | 6100  | Level 5     |
| Basic Fire Station  | 2    | 2    | 10     | 12     | 11000 | Level 5     |
| Deluxe Fire Station | 4    | 2    | 22     | 22     | 42100 | Level 5     |

### Police
| Buidling Name         | X    | Y    | CoverX | CoverY | Price | Requirement |
| :---                  | ---: | ---: | ---:   | ---:   | ---:  | :---        |
| Small Police Station  | 1    | 1    | 6      | 8      | 10100 | Level 12    |
| Basic Police Station  | 2    | 2    | 12     | 12     | 18800 | Level 12    |
| Police Precinct       | 4    | 2    | 24     | 24     | 72100 | Level 12    |

### Health
| Buidling Name       | X    | Y    | CoverX | CoverY | Price | Requirement |
| :---                | ---: | ---: | ---:   | ---:   | ---:  | :---        |
| Small Health Clinic | 1    | 1    | 8      | 8      | 12300 | Level 16    |
| Health Clinic       | 2    | 2    | 12     | 12     | 23200 | Level 16    |
| Hospital            | 4    | 2    | 24     | 24     | 64200 | Level 16    |

## Specializations

### Park
| Building Name                 | X    | Y    | CoverX | CoverY | Simoleons | SimCash | Boost Rate | Requirement        |
| :---                          | ---: | ---: | ---:   | ---:   | ---:      | ---:    | ---:       | :---               |
| Small Fountain Park           | 1    | 1    | 8      | 8      | 4000      | 0       | 5%         | Level 3            |
| Modern Art Park               | 1    | 1    | 8      | 6      | 5000      | 0       | 10%        | Level 3            |
| Reflecting Pool Park          | 1    | 2    | 8      | 6      | 6000      | 0       | 20%        | 2000 Populations   |
| Peaceful Park                 | 1    | 2    | 6      | 10     | 8000      | 0       | 25%        | 5000 Populations   |
| Urban Plaza                   | 1    | 2    | 8      | 6      | 12000     | 0       | 20%        | 9000 Populations   |
| Sculpture Garden              | 1    | 2    | 6      | 8      | 16000     | 0       | 30%        | 15000 Populations  |
| Row of Trees                  | 2    | 1    | 12     | 4      | 20000     | 0       | 30%        | 18000 Populations  |
| Soccer Field                  | 2    | 2    | 8      | 8      | 24000     | 0       | 25%        | 29000 Populations  |
| Sakura Park                   | 1    | 1    | 8      | 8      | 60000     | 0       | 25%        | 180000 Populations |
| Jogging Path                  | 2    | 2    | 8      | 8      | 28000     | 0       | 30%        | 39000 Populations  |
| Plumbob Park                  | 1    | 1    | 6      | 8      | 0         | 140     | 20%        | Level 3            |
| Larry the Llama               | 2    | 2    | 8      | 8      | 0         | 280     | 25%        | 2000 Populations   |
| World's Largest Ball of Twine | 2    | 2    | 6      | 8      | 0         | 300     | 30%        | 9000 Populations   |
| Water Park Playground         | 2    | 2    | 8      | 8      | 0         | 400     | 25%        | 49000 Populations  |
| Giant Garden Gnome            | 2    | 2    | 8      | 8      | 0         | 500     | 30%        | 59000 Populations  |
| Basketball Court              | 2    | 1    | 10     | 6      | 0         | 600     | 30%        | 95000 Populations  |
| Dolly the Dinosaur            | 3    | 2    | 10     | 8      | 0         | 700     | 40%        | 105000 Populations |
| Skate Park                    | 1    | 2    | 6      | 12     | 0         | 800     | 40%        | 130000 Populations |

### Education
| Building Name           | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement              |
| :---                    | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---                     |
| Department of Education | 2    | 3    | 8      | 8      | 40000     | 0    | 25%        | Level 10                 |
| Grade School            | 2    | 3    | 8      | 10     | 0         | 3    | 25%        | Deptartment of Education |
| Public Library          | 2    | 2    | 12     | 8      | 0         | 5    | 25%        | Deptartment of Education |
| High School             | 4    | 2    | 14     | 12     | 0         | 7    | 30%        | Public Library           |
| Community College       | 4    | 2    | 16     | 16     | 0         | 10   | 40%        | High School              |
| University              | 4    | 4    | 22     | 22     | 0         | 25   | 50%        | Community College        |

### Transportation
| Building Name                | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement             |
| :---                         | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---                    |
| Department of Transportation | 2    | 3    | 8      | 8      | 40000     | 0    | 30%        | Level 10                |
| Bus Terminal                 | 2    | 2    | 16     | 12     | 0         | 4    | 20%        | Department of Transport |
| Airship Hangar               | 3    | 2    | 16     | 16     | 0         | 10   | 30%        | Department of Transport |
| Balloon Park                 | 2    | 2    | 16     | 16     | 0         | 14   | 40%        | Department of Transport |
| Heliport                     | 2    | 2    | 18     | 18     | 0         | 20   | 50%        | Department of Transport |

### Gambling
| Building Name          | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement      |
| :---                   | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---             |
| Gambling HQ            | 2    | 2    | 8      | 8      | 70000     | 0    | 20%        | Level 25         |
| Gambling House         | 2    | 2    | 8      | 10     | 0         | 3    | 20%        | Gambling HQ      |
| Sleek Casino           | 2    | 2    | 12     | 10     | 0         | 5    | 25%        | Gambling HQ      |
| Sleek Casino Tower     | 2    | 2    | 12     | 10     | 0         | 6    | 25%        | Sleek Casino     |
| Sci-fi Casino          | 2    | 2    | 12     | 12     | 0         | 8    | 30%        | Gambling HQ      |
| Sci-fi Casino Tower    | 2    | 2    | 16     | 12     | 0         | 9    | 30%        | Sci-fi Casino    |
| Luxurious Casino       | 2    | 2    | 16     | 16     | 0         | 10   | 50%        | Gambling HQ      |
| Luxurious Casino Tower | 2    | 2    | 16     | 16     | 0         | 12   | 60%        | Luxurious Casino |

### Culture
| Building Name         | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement                                  |
| :---                  | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---                                         |
| Department of Culture | 2    | 2    | 8      | 8      | 80000     | 0    | 20%        | Level 30                                     |
| Tower of Pisa         | 2    | 2    | 14     | 16     | 0         | 12   | 20%        | Department of Culture                        |
| Big Ben               | 3    | 2    | 16     | 14     | 0         | 14   | 30%        | Department of Culture                        |
| Arc de Triomphe       | 2    | 3    | 14     | 16     | 0         | 16   | 40%        | Department of Culture                        |
| Brandenburg Gate      | 2    | 4    | 16     | 14     | 0         | 18   | 40%        | Department of Culture                        |
| Empire State Building | 2    | 4    | 16     | 16     | 0         | 20   | 40%        | Department of Culture                        |
| Statue of Liberty     | 3    | 3    | 14     | 16     | 0         | 30   | 60%        | Department of Culture</br>200000 Populations |
| Washington Monument   | 2    | 2    | 18     | 18     | 0         | 40   | 60%        | Department of Culture</br>250000 Populations |

### Entertainment
| Building Name      | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement                             |
| :---               | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---                                    |
| Entertainment HQ   | 2    | 2    | 8      | 8      | 60000     | 0    | 20%        | Level 20                                |
| Hotel              | 2    | 2    | 10     | 10     | 0         | 8    | 25%        | Entertainment HQ                        |
| Amphitheater       | 2    | 2    | 10     | 10     | 0         | 9    | 25%        | Entertainment HQ                        |
| Expo Center        | 6    | 4    | 18     | 16     | 0         | 10   | 40%        | Entertainment HQ                        |
| Stadium            | 6    | 4    | 20     | 20     | 0         | 18   | 50%        | Entertainment HQ                        |
| Sydney Opera House | 6    | 4    | 20     | 20     | 0         | 25   | 60%        | Entertainment HQ</br>110000 Populations |
| Ferris Wheel       | 3    | 3    | 20     | 20     | 0         | 28   | 60%        | Entertainment HQ</br>170000 Populations |
| Sumo Hall          | 5    | 4    | 16     | 16     | 0         | 50   | 40%        | Entertainment HQ</br>230000 Populations |

### Worship
| Building Name | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement |
| :---          | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---        |
| Church        | 3    | 2    | 12     | 12     | 0         | 40   | 40%        | Level 40    |
| Mosque        | 3    | 2    | 12     | 12     | 0         | 40   | 40%        | Level 40    |
| Temple        | 3    | 2    | 12     | 12     | 0         | 40   | 40%        | Level 40    |
| Modern Temple | 3    | 2    | 12     | 12     | 0         | 40   | 40%        | Level 40    |

### Beach
| Building Name            | X    | Y    | CoverX | CoverY | Simoleons | Key  | Boost Rate | Requirement     |
| :---                     | ---: | ---: | ---:   | ---:   | ---:      | ---: | ---:       | :---            |
| Surfer Beach             | 1    | 3    | 3      | 12     | 20000     | 0    | 10%        | Level 15        |
| Volleyball Court         | 2    | 2    | 4      | 12     | 0         | 5    | 10%        | Surfer Beach    |
| Lifeguard Tower          | 1    | 4    | 4      | 12     | 0         | 10   | 20%        | Surfer Beach    |
| Beachfront Shopping Mall | 4    | 2    | 7      | 12     | 0         | 16   | 20%        | Level 15        |
| Relaxing Beach           | 1    | 4    | 3      | 20     | 0         | 12   | 20%        | Surfer Beach    |
| Merman Statue            | ?4   | ?7   | 6      | 20     | 0         | 20   | 20%        | Lifeguard Tower |
| Carousel                 | 2    | 2    | 4      | 20     | 0         | 7    | 10%        | Level 15        |
| Bluebeard's Pirate Ship  | ?3   | ?3   | 5      | 12     | 0         | 13   | 20%        | Carousel        |
| Water Park               | ?4   | ?3   | 7      | 30     | 0         | 18   | 10%        | Carousel        |
| Aquarium                 | ?4   | ?3   | 7      | 20     | 0         | 25   | 20%        | Water Park      |
| Astro-Twirl Rocket Ride  | ?6   | ?3   | 9      | 20     | 0         | 32   | 30%        | Water Park      |
| Lighthouse               | 2    | 6    | 4      | 20     | 0         | 15   | 20%        | Level 15        |
| Sailorman's Pie          | 2    | 8    | 6      | 20     | 0         | 22   | 30%        | Lighthouse      |
| Luxury Boat Marina       | 6    | 6    | 9      | 30     | 0         | 50   | 30%        | Sailorman's Pie |

### Mountain
| Bulding Name               | X    | Y    | CoverX | CoverY | Simoleons | SimCash | Key  | Boost Rate | Requirement                |
| :---                       | ---: | ---: | ---:   | ---:   | ---:      | ---:    | ---: | ---:       | :---                       |
| City Name Sign             | 8    | 2    | 11     | 30     | 0         | 700     | 0    | 60%        | Level 23                   |
| Cozy Cottages              | 1    | 3    | 3      | 12     | 10000     | 0       | 0    | 20%        | Level 23                   |
| Mountainside Cottages      | 1    | 4    | 4      | 12     | 0         | 0       | 4    | 25%        | Cozy Cottages              |
| Hiker's Cottages           | 1    | 5    | 5      | 20     | 0         | 0       | 8    | 30%        | Mountainside Cottages      |
| Alpine Cafe                | 2    | 5    | 7      | 30     | 0         | 0       | 13   | 35%        | Mountainside Cottages      |
| Mountainside Train Station | 4    | 1    | 6      | 20     | 0         | 0       | 15   | 40%        | Mountainside Cottages      |
| Alpine Vineyard            | 4    | 5    | 7      | 30     | 0         | 0       | 20   | 45%        | Mountainside Cottages      |
| Mountain Lift              | 2    | 6    | 4      | 20     | 30000     | 0       | 0    | 30%        | Mountainside Cottages      |
| Halfpipe                   | 2    | 6    | 5      | 20     | 0         | 0       | 10   | 35%        | Mountain Lift              |
| Ski jumping Hill           | 2    | 6    | 6      | 20     | 0         | 0       | 15   | 40%        | Mountain Lift              |
| Ski Hotel                  | 4    | 4    | 7      | 20     | 0         | 0       | 25   | 45%        | Mountain Lift              |
| Ski Resort                 | 6    | 6    | 8      | 30     | 0         | 0       | 30   | 50%        | Mountain Lift              |
| Communication Tower        | 2    | 3    | 6      | 30     | 0         | 280     | 0    | 30%        | Level 23                   |
| Observatory                | 4    | 4    | 5      | 20     | 0         | 0       | 14   | 40%        | Mountainside Train Station |
| Mount SimCity              | 4    | 6    | 7      | 20     | 0         | 0       | 25   | 45%        | Mountainside Train Station |
| Mountain Palace            | 6    | 5    | 8      | 30     | 0         | 0       | 30   | 50%        | Mountainside Train Station |
| Castle                     | 7    | 5    | 8      | 30     | 0         | 0       | 35   | 55%        | Mountainside Train Station |

